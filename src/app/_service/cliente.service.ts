import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class ClienteService {

  constructor(private http: HttpClient) { }


  enviarDatosCliente(formaAddress: any) {

    console.log('llego formulario a capa de servicio', formaAddress);

    return this.http.post('http://localhost:3000/clientes/guardarCliente', formaAddress)
      .pipe(map(res => res))
      .subscribe(result => {
        console.log('resultado', result);
      }, err => {
        console.log('error', err);
      }, () => { });



  }
}

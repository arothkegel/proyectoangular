import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ClienteService } from '../_service/cliente.service';

@Component({
  selector: 'app-cliente',
  templateUrl: './cliente.component.html',
  styleUrls: ['./cliente.component.css']
})
export class ClienteComponent {
  addressForm = this.fb.group({
    empresa: null,
    nombre: [null, Validators.required],
    apelido: [null, Validators.required],
    direccion: [null, Validators.required],
    ciudad: [null, Validators.required],
    comuna: [null, Validators.required],
    codigoPostal: [null, Validators.compose([
      Validators.required, Validators.minLength(5), Validators.maxLength(5)])
    ],
  });

  hasUnitNumber = false;

  comunas = [
    { name: 'Santiago', abbreviation: 'SG' },
    { name: 'Ñuñoa', abbreviation: 'ÑÑ' },
    { name: 'Recoleta', abbreviation: 'RC' }
  ];

  constructor(private fb: FormBuilder, private servicio: ClienteService) { }

  onSubmit() {
    // alert('Thanks!');
    this.servicio.enviarDatosCliente(this.addressForm.value);

  }
}
